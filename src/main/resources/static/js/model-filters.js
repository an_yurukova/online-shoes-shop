const addFilter = (type, element) => {

	element.onclick = (e) => {
		const searchParams = new URLSearchParams(window.location.search);
		if (searchParams.getAll(type).includes(e.target.value)) {
			return;
		}
		
		searchParams.append(type, e.target.value);
		const href = window.location.href.replace(window.location.search, "") + "?" + searchParams;
		window.location.href = href;

	};
}

const addFilters = (type) => {
	document.querySelectorAll(`[data-type="${type}"]`).forEach(e => addFilter(type, e));
}

addFilters("type");
addFilters("size");
addFilters("color");