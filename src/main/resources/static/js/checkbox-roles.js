const userList = document.getElementById('roleList');
const searchBar = document.getElementById('searchInputUsers');

const allUsers = [];


const renderRow = (user) => {

	const buttonActive = `<button type="button" onclick="updateUser(${user.id}, ${!user.active})"
					class="btn btn-sm btn-outline-secondary rounded" >${user.active ? "Deactivate" : "Activate"}</button>`


	return `<tr>
	   <td class="text-nowrap align-middle">${user.id}</td>
	   <td class="text-nowrap align-middle">${user.username}</td>
	   <td class="text-nowrap align-middle">${user.firstName} ${user.lastName}</td>
	   <td class="text-nowrap align-middle">${user.email}</td>
       <td class="text-nowrap align-middle">Role

	      <div>
		     <input type="checkbox" id="user" name="admin"
			value="user"> <label for="user">User</label>
	      </div>
	      <div>
		  <input type="checkbox" id="admin" name="admin"
			value="admin"> <label for="admin">Admin</label>
	      </div>
       </td>
	   <td class="text-center align-middle">
			<div class="btn-group">			
				${buttonActive}			    
			</div>
	   </td>

	</tr>`;
}


const updateUser = async (id, active) => {
	const response = await fetch(`/api/users/${id}`, {
		method: "PUT",
		headers: {
			'Content-Type': 'application/json'		
		},
		body: JSON.stringify({ active })
	});
	if (response.ok) {
		renderUsers();
	}

}



const renderUsers = async () => {
	const response = await fetch("/api/users");
	const users = await response.json();


	const tbody = document.getElementById("userList");
	tbody.innerHTML = users.map(renderRow).reduce((a, b) => a + b, "");

}

renderUsers();

fetch("http://localhost:8080/api/users")
	.then(response => response.json()
		.then(data => {
			for (let user of data) {
				allUsers.push(user);
			}
			console.log(allUsers);
		}));


searchBar.addEventListener('keyup', () => {
	const searchingCharacters = searchBar.value.toLowerCase();
	let filteredUsers = allUsers.filter(user => {
		return user.username.toLowerCase().includes(searchingCharacters);

	})
	console.log(filteredUsers);
	displayUsers(filteredUsers);
})


const displayUsers = (users) => {
	const tbody = document.getElementById("userList");
	tbody.innerHTML = users.map(renderRow).reduce((a, b) => a + b, "");
}
