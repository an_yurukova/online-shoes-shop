const shoesModelSizeList = document.getElementById('shoesModelSizeList');
const searchBar = document.getElementById('searchInput');

const allModels = [];

const renderRow = (size) => `
	<tr >
		<td class="text-nowrap align-middle">${size.id}</td>
		<td class="text-nowrap align-middle">${size.model.name}</td>
		<td class="text-nowrap align-middle">${size.model.type}</td>
		<td class="text-nowrap align-middle">${size.model.sex}</td>
	    <td class="btn rounded-circle p-3 m-2" style="background-color:${size.model.colorHex}"></td>
        <td class="text-nowrap align-middle">${size.model.price}</td>
		<td class="text-nowrap align-middle">${size.model.createdOn}</td>
		<td class="text-nowrap align-middle">${size.model.createdBy}</td>
		<td class="text-nowrap align-middle">Image</td>
		<td class="text-nowrap align-middle">${size.size}</td>
		<td class="text-nowrap align-middle">${size.quantity}</td>
	
		<td class="text-center align-middle">
			<div class="btn-group">
				<a type="button"
					href="/shoes-model-sizes/${size.id}"
					class="btn btn-sm btn-outline-secondary rounded" style="margin-right: 5px;">Edit</a>
				<button type="button"
					onclick="deleteSize(${size.id})"
					class="btn btn-sm btn-outline-secondary rounded" style="margin-left: 5px;">Delete</button>
			</div>
		</td>
	</tr>`;


const deleteSize = async (id) => {
	const response = await fetch(`/api/shoes-model-sizes/${id}`, {
		method: "DELETE"
	});
	if (response.ok) {
		renderModels();
	}

}



const renderModels = async () => {
	const response = await fetch("/api/shoes-model-sizes");
	const models = await response.json();


	const tbody = document.getElementById("shoesModelSizeList");
	tbody.innerHTML = models.map(renderRow).reduce((a, b) => a + b, "");

}

renderModels();

fetch("http://localhost:8080/api/shoes-model-sizes")
	.then(response => response.json()
		.then(data => {
			for (let model of data) {
				allModels.push(model);
			}
			console.log(allModels);
		}));


searchBar.addEventListener('keyup', () => {
	const searchingCharacters = searchBar.value.toLowerCase();
	let filteredModels = allModels.filter(size => {
		return size.model.name.toLowerCase().includes(searchingCharacters);

	})
	console.log(filteredModels);
	displayShoesModelSizes(filteredModels);
})


const displayShoesModelSizes = (models) => {
	const tbody = document.getElementById("shoesModelSizeList");
	tbody.innerHTML = models.map(renderRow).reduce((a, b) => a + b, "");
}

