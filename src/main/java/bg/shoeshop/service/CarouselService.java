package bg.shoeshop.service;

public interface CarouselService {
  public String firstImage();

  public String secondImage();

  public String thirdImage();
}
