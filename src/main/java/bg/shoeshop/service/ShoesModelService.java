package bg.shoeshop.service;

import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import bg.shoeshop.model.service.ShoesModelServiceModel;
import bg.shoeshop.model.view.ShoesModelViewModel;
import bg.shoeshop.web.ShoesModelsFilter;
import java.io.IOException;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ShoesModelService {
  ShoesModel addModel(ShoesModelServiceModel ShoesModelServiceModel) throws IOException;

  Page<ShoesModelViewModel> getAllAvailableShoesModels(
      ShoesModelsFilter shoesModelsFilter, Pageable pageable);

  Page<ShoesModelViewModel> getAllAvailableShoesModelsForSex(
      Sex sex, Pageable pageable, ShoesModelsFilter shoesModelsFilter);

  ShoesModelViewModel getById(Long id);

  ShoesModel getModelByName(String name);

  boolean checkIfExists(String name);

  List<Type> getAllAvailableModelTypes();

  List<Integer> getAllAvailableSizes();

  List<String> getAllAvailableColorHexes();

  List<Type> getAllAvailableModelTypesForSex(Sex sex);

  List<String> getAllAvailableColorHexesForSex(Sex sex);

  List<Integer> getAllAvailableSizesForSex(Sex sex);

  List<Integer> getAllAvailableSizesForShoesModel(Long id);
}
