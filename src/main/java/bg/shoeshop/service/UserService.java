package bg.shoeshop.service;

import bg.shoeshop.model.binding.UserProfileBindingModel;
import bg.shoeshop.model.binding.UserUpdateBindingModel;
import bg.shoeshop.model.service.UserServiceModel;
import bg.shoeshop.model.view.ShoesModelSizeViewModel;
import bg.shoeshop.model.view.UserViewModel;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import org.springframework.security.core.userdetails.User;

public interface UserService {

  boolean usernameExists(String username);

  void registerAndLogin(UserServiceModel userServiceModel);

  List<UserViewModel> getAllUsers();

  void addPermission(Long id);

  //  void deleteById(Long id);

  UserProfileBindingModel getUser();

  User getSpringUser();

  void changeUserInfo(UserServiceModel userServiceModel);

  Set<ShoesModelSizeViewModel> getAllFavourites();

  void addToShoppingCart(Long id);

  void addToFavourites(Long id);

  Set<ShoesModelSizeViewModel> getShoppingCart();

  BigDecimal getTotalAmount();

  void deleteFromFavouritesById(Long id);

  void deleteFromShoppingCartById(Long id);

  void update(Long id, UserUpdateBindingModel userUpdateBindingModel);

  void updateRoles(Long id, String roleName);

  void deleteFromFavourites();
}
