package bg.shoeshop.service.impl;

import bg.shoeshop.model.entity.Authority;
import bg.shoeshop.model.entity.Role;
import bg.shoeshop.model.entity.User;
import bg.shoeshop.repository.UserRepository;
import java.util.Collection;
import java.util.stream.Stream;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;;

@Service
@Transactional
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    return userRepository
        .findByUsername(username)
        .map(this::mapToUserDetails)
        .orElseThrow(
            () -> new UsernameNotFoundException("User with name: " + username + " not found!"));
  }

  private UserDetails mapToUserDetails(User user) {

    Stream<String> roles = user.getRoles().stream().map(Role::getName).map("ROLE_"::concat);
    Stream<String> authorities =
        user.getRoles()
            .stream()
            .map(Role::getAuthorities)
            .flatMap(Collection::stream)
            .map(Authority::getName)
            .distinct();

    String[] auth = Stream.concat(roles, authorities).toArray(String[]::new);

    UserDetails userr =
        org.springframework.security.core.userdetails.User.withUsername(user.getUsername())
            .password(String.valueOf(user.getPassword()))
            .authorities(auth)
            .build();

    return new UserPrincipal(user.getId(), userr);
  }

  public static class UserPrincipal extends org.springframework.security.core.userdetails.User {

    /** */
    private static final long serialVersionUID = -24677778435979057L;

    private Long id;

    public UserPrincipal(Long id, UserDetails user) {
      super(
          user.getUsername(),
          user.getPassword(),
          user.isEnabled(),
          user.isAccountNonExpired(),
          user.isCredentialsNonExpired(),
          user.isAccountNonLocked(),
          user.getAuthorities());
      this.id = id;
    }

    public Long getId() {
      return this.id;
    }
  }
}
