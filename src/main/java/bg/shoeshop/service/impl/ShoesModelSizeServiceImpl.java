package bg.shoeshop.service.impl;

import bg.shoeshop.exception.ProductNotFoundException;
import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.entity.ShoesModelSize;
import bg.shoeshop.model.service.ShoesServiceModel;
import bg.shoeshop.repository.ShoesModelRepository;
import bg.shoeshop.repository.ShoesModelSizeRepository;
import bg.shoeshop.service.ShoesModelSizeService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShoesModelSizeServiceImpl implements ShoesModelSizeService {

  private final ShoesModelRepository shoesModelRepository;
  private final ModelMapper modelMapper;
  private final ShoesModelSizeRepository shoesModelSizeRepository;

  @Override
  public List<ShoesServiceModel> findAll() {
    return shoesModelSizeRepository
        .findAll()
        .stream()
        .map(sms -> modelMapper.map(sms, ShoesServiceModel.class))
        .collect(Collectors.toList());
  }

  @Override
  public ShoesServiceModel getShoesModelSizeById(Long id) {
    ShoesModelSize size =
        shoesModelSizeRepository
            .findById(id)
            .orElseThrow(() -> new ProductNotFoundException(String.valueOf(id)));
    return modelMapper.map(size, ShoesServiceModel.class);
  }

  @Override
  public void addSize(Long modelId, int size, int quantity) {
    Optional<ShoesModelSize> model = shoesModelSizeRepository.findByModelIdAndSize(modelId, size);
    if (model.isPresent()) {
      model.get().setQuantity(model.get().getQuantity() + quantity);
    }
    ShoesModelSize newSize =
        new ShoesModelSize(shoesModelRepository.findById(modelId).get(), size, quantity);
    shoesModelSizeRepository.save(newSize);
  }

  @Override
  public void deleteById(Long id) {

    shoesModelSizeRepository.deleteById(id);
  }

  @Override
  public Set<ShoesModelSize> findAllByModelId(Long id) {
    return shoesModelSizeRepository.findAllByModelId(id);
  }

  @Override
  public void editShoesModelSizeInfo(Long id, BigDecimal price, int quantity) {
    ShoesModelSize size =
        shoesModelSizeRepository
            .findById(id)
            .orElseThrow(() -> new ProductNotFoundException(String.valueOf(id)));
    ShoesModel model = shoesModelRepository.findByName(size.getModel().getName()).get();
    model.setPrice(price);
    shoesModelRepository.save(model);
    size.setModel(model);
    size.setQuantity(quantity);
    shoesModelSizeRepository.save(size);
  }
}
