package bg.shoeshop.service.impl;

import bg.shoeshop.exception.ProductNotFoundException;
import bg.shoeshop.model.binding.UserProfileBindingModel;
import bg.shoeshop.model.binding.UserUpdateBindingModel;
import bg.shoeshop.model.entity.Role;
import bg.shoeshop.model.entity.ShoesModelSize;
import bg.shoeshop.model.entity.User;
import bg.shoeshop.model.service.UserServiceModel;
import bg.shoeshop.model.view.ShoesModelSizeViewModel;
import bg.shoeshop.model.view.UserViewModel;
import bg.shoeshop.repository.RoleRepository;
import bg.shoeshop.repository.ShoesModelSizeRepository;
import bg.shoeshop.repository.UserRepository;
import bg.shoeshop.service.UserService;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {
  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final ShoesModelSizeRepository shoesModelSizeRepository;
  private final ModelMapper modelMapper;
  private final PasswordEncoder passwordEncoder;
  private final UserDetailsService userDetailsService;

  @Override
  public boolean usernameExists(String username) {
    return userRepository.findByUsername(username).isPresent();
  }

  @Override
  public void registerAndLogin(UserServiceModel userServiceModel) {
    User user = modelMapper.map(userServiceModel, User.class);
    user.setPassword(
        passwordEncoder.encode(String.valueOf(userServiceModel.getPassword())).toCharArray());
    Role role =
        roleRepository
            .findByName("USER")
            .orElseThrow(
                () -> new IllegalStateException("USER role not found. Please create the role."));
    user.getRoles().add(role);
    user = userRepository.save(user);

    UserDetails principal = userDetailsService.loadUserByUsername(user.getUsername());

    Authentication authentication =
        new UsernamePasswordAuthenticationToken(
            principal, user.getPassword(), principal.getAuthorities());

    SecurityContextHolder.getContext().setAuthentication(authentication);
  }

  @Override
  public List<UserViewModel> getAllUsers() {
    List<User> users = userRepository.findAll(Sort.by(Sort.Direction.ASC, "username"));

    return users
        .stream()
        .map(u -> modelMapper.map(u, UserViewModel.class))
        .collect(Collectors.toList());
  }

  @Override
  public void addPermission(Long id) {
    Role role =
        roleRepository
            .findByName("ADMIN")
            .orElseThrow(
                () -> new IllegalStateException("ADMIN role not found. Please create the role."));

    userRepository.findById(id).get().getRoles().add(role);
  }

  @Override
  public UserProfileBindingModel getUser() {
    User user = userRepository.findByUsername(getSpringUser().getUsername()).get();
    return modelMapper.map(user, UserProfileBindingModel.class);
  }

  @Override
  public org.springframework.security.core.userdetails.User getSpringUser() {

    return (org.springframework.security.core.userdetails.User)
        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  @Override
  public void changeUserInfo(UserServiceModel userServiceModel) {
    User user =
        userRepository
            .findByUsername(userServiceModel.getUsername())
            .orElseThrow(() -> new NoSuchElementException("User not found."));
    user.setFirstName(userServiceModel.getFirstName());
    user.setLastName(userServiceModel.getLastName());
    user.setEmail(userServiceModel.getEmail());
    userRepository.save(user);
  }

  @Override
  public Set<ShoesModelSizeViewModel> getAllFavourites() {
    String username = getSpringUser().getUsername();
    return userRepository
        .findByUsername(username)
        .get()
        .getFavourites()
        .stream()
        .map(f -> modelMapper.map(f, ShoesModelSizeViewModel.class))
        .collect(Collectors.toSet());
  }

  @Override
  public void addToShoppingCart(Long id) {
    String username = getSpringUser().getUsername();
    ShoesModelSize shoesModelSize =
        shoesModelSizeRepository
            .findById(id)
            .orElseThrow(() -> new ProductNotFoundException("Product with id: " + id));
    userRepository.findByUsername(username).get().getShoppingCart().add(shoesModelSize);
  }

  @Override
  public void addToFavourites(Long id) {
    String username = getSpringUser().getUsername();
    ShoesModelSize shoesModelSize =
        shoesModelSizeRepository
            .findById(id)
            .orElseThrow(() -> new ProductNotFoundException("Product with id: " + id));
    userRepository.findByUsername(username).get().getFavourites().add(shoesModelSize);
  }

  @Override
  public Set<ShoesModelSizeViewModel> getShoppingCart() {
    String username = getSpringUser().getUsername();
    return userRepository
        .findByUsername(username)
        .get()
        .getShoppingCart()
        .stream()
        .map(smz -> modelMapper.map(smz, ShoesModelSizeViewModel.class))
        .collect(Collectors.toSet());
  }

  @Override
  public BigDecimal getTotalAmount() {
    String username = getSpringUser().getUsername();
    return userRepository.getShoppingCartTotalAmount(
        userRepository.findByUsername(username).get().getId());
  }

  @Override
  public void deleteFromShoppingCartById(Long id) {
    String username = getSpringUser().getUsername();
    ShoesModelSize sms =
        shoesModelSizeRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
    userRepository.findByUsername(username).get().getShoppingCart().remove(sms);
  }

  @Override
  public void deleteFromFavouritesById(Long id) {
    // TODO Auto-generated method stub

  }

  @Override
  public void update(Long id, UserUpdateBindingModel userUpdateBindingModel) {
    userRepository.findById(id).get().setActive(userUpdateBindingModel.isActive());
  }

  @Override
  public void updateRoles(Long id, String roleName) {
    Role role = roleRepository.findByName(roleName).get();
    List<Role> userRoles = userRepository.findById(id).get().getRoles();

    if (!userRoles.contains(role)) {
      userRoles.add(role);
    } else {
      userRoles.remove(role);
    }
  }

  @Override
  public void deleteFromFavourites() {
    String username = getSpringUser().getUsername();
    userRepository.findByUsername(username).get().getFavourites().clear();
    System.out.println("all favourites deleted");
  }

  //
  //  @Override
  //  public void deleteById(Long id) {}
}
