package bg.shoeshop.service.impl;

import bg.shoeshop.service.CarouselService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CarouselServiceImpl implements CarouselService {

  private List<String> images = new ArrayList<>();

  public CarouselServiceImpl(@Value("${carousel.images}") List<String> images) {
    this.images = images;
  }

  @PostConstruct
  public void afterInitializing() {
    System.out.println("afterInitializing");
    if (images.size() < 3) {
      throw new IllegalArgumentException("Sorry, you must configure at least 3 images!");
    }
  }

  @Override
  public String firstImage() {

    return images.get(0);
  }

  @Override
  public String secondImage() {
    return images.get(1);
  }

  @Override
  public String thirdImage() {
    return images.get(2);
  }

  @Scheduled(cron = "${carousel.refresh-cron}")
  public void refresh() {
    System.out.println("shuffling images");
    Collections.shuffle(images);
  }
}
