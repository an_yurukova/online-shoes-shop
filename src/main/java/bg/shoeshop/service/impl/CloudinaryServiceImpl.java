package bg.shoeshop.service.impl;

import bg.shoeshop.service.CloudinaryService;
import com.cloudinary.Cloudinary;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CloudinaryServiceImpl implements CloudinaryService {

  private final Cloudinary cloudinary;

  public CloudinaryServiceImpl(Cloudinary cloudinary) {
    this.cloudinary = cloudinary;
  }

  @Override
  public String uploadImage(MultipartFile multipartFile) throws IOException {
    File file = File.createTempFile("tmp", multipartFile.getOriginalFilename());
    multipartFile.transferTo(file);

    return cloudinary.uploader().upload(file, new HashMap<>()).get("url").toString();
  }
}
