package bg.shoeshop.service.impl;

import bg.shoeshop.exception.ProductNotFoundException;
import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import bg.shoeshop.model.service.ShoesModelServiceModel;
import bg.shoeshop.model.view.ShoesModelViewModel;
import bg.shoeshop.repository.ShoesModelRepository;
import bg.shoeshop.repository.UserRepository;
import bg.shoeshop.service.ShoesModelService;
import bg.shoeshop.service.UserService;
import bg.shoeshop.web.ShoesModelsFilter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class ShoesModelServiceImpl implements ShoesModelService {

  private final ShoesModelRepository shoesModelRepository;
  private final UserRepository userRepository;
  private final UserService userService;
  private final ModelMapper modelMapper;

  @Override
  public ShoesModel addModel(ShoesModelServiceModel shoesModelServiceModel) throws IOException {
    ShoesModel model = modelMapper.map(shoesModelServiceModel, ShoesModel.class);
    model.setCreatedOn(LocalDate.now());
    User user = userService.getSpringUser();
    model.setCreatedBy(userRepository.findByUsername(user.getUsername()).get());
    return shoesModelRepository.save(model);
  }

  @RequiredArgsConstructor
  private class ShoesModelsSpecifications {

    private final ShoesModelsFilter shoesModelsFilter;

    private Specification<ShoesModel> getTypeSpecification() {
      return shoesModelsFilter.getType().isEmpty()
          ? (r, q, cb) -> null
          : (r, q, cb) -> r.get("type").in(shoesModelsFilter.getType());
    }

    private Specification<ShoesModel> getSizeSpecification() {
      return shoesModelsFilter.getSize().isEmpty()
          ? (r, q, cb) -> null
          : (r, q, cb) -> r.join("sizes").get("size").in(shoesModelsFilter.getSize());
    }

    private Specification<ShoesModel> getColorSpecification() {
      return shoesModelsFilter.getColour().isEmpty()
          ? (r, q, cb) -> null
          : (r, q, cb) -> r.get("color").in(shoesModelsFilter.getColour());
    }

    private Specification<ShoesModel> getComposite() {
      return getSizeSpecification().and(getTypeSpecification()).and(getColorSpecification());
    }

    private Specification<ShoesModel> getSexSpecification(Sex sex) {
      return (r, q, cb) -> cb.equal(r.get("sex"), sex);
    }
  }

  @Override
  public Page<ShoesModelViewModel> getAllAvailableShoesModels(
      ShoesModelsFilter shoesModelsFilter, Pageable pageable) {
    System.out.println("inside getAllShoesModels");

    return shoesModelRepository
        .findAll(new ShoesModelsSpecifications(shoesModelsFilter).getComposite(), pageable)
        .map(m -> modelMapper.map(m, ShoesModelViewModel.class));
  }

  @Override
  public Page<ShoesModelViewModel> getAllAvailableShoesModelsForSex(
      Sex sex, Pageable pageable, ShoesModelsFilter shoesModelsFilter) {

    ShoesModelsSpecifications specs = new ShoesModelsSpecifications(shoesModelsFilter);

    return shoesModelRepository
        .findAll(specs.getComposite().and(specs.getSexSpecification(sex)), pageable)
        .map(m -> modelMapper.map(m, ShoesModelViewModel.class));
  }

  @Override
  public List<Type> getAllAvailableModelTypes() {
    return shoesModelRepository.findAllAvailableModelTypes();
  }

  @Override
  public ShoesModelViewModel getById(Long id) {
    ShoesModel shoesModel = shoesModelRepository.getOne(id);
    ShoesModelViewModel shoesModelViewModel =
        modelMapper.map(shoesModel, ShoesModelViewModel.class);

    return shoesModelViewModel;
  }

  @Override
  public ShoesModel getModelByName(String name) {

    return shoesModelRepository
        .findByName(name)
        .orElseThrow(() -> new ProductNotFoundException(name));
  }

  @Override
  public boolean checkIfExists(String name) {
    return shoesModelRepository.findByName(name).isPresent();
  }

  @Override
  public List<Integer> getAllAvailableSizes() {
    return shoesModelRepository.findAllAvailableSizes();
  }

  @Override
  public List<String> getAllAvailableColorHexes() {
    return shoesModelRepository.findAllAvailableColorHexes();
  }

  @Override
  public List<Type> getAllAvailableModelTypesForSex(Sex sex) {
    return shoesModelRepository.findAllAvailableModelTypesForSex(sex);
  }

  @Override
  public List<String> getAllAvailableColorHexesForSex(Sex sex) {
    return shoesModelRepository.findAllAvailableColorHexesForSex(sex);
  }

  @Override
  public List<Integer> getAllAvailableSizesForSex(Sex sex) {
    return shoesModelRepository.findAllAvailableSizesForSex(sex);
  }

  @Override
  public List<Integer> getAllAvailableSizesForShoesModel(Long id) {
    return shoesModelRepository.findAllAvailableSizesForShoesModel(id);
  }
}
