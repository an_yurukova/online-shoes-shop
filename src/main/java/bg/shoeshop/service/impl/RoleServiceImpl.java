package bg.shoeshop.service.impl;

import bg.shoeshop.repository.RoleRepository;
import bg.shoeshop.service.RoleService;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class RoleServiceImpl implements RoleService {

  private final RoleRepository roleRepository;

  @Override
  public Set<String> getAllRoles() {

    return roleRepository.findAll().stream().map(r -> r.getName()).collect(Collectors.toSet());
  }
}
