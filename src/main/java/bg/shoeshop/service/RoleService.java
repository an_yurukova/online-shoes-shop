package bg.shoeshop.service;

import java.util.Set;

public interface RoleService {

  Set<String> getAllRoles();
}
