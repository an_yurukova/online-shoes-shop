package bg.shoeshop.service;

import bg.shoeshop.model.entity.ShoesModelSize;
import bg.shoeshop.model.service.ShoesServiceModel;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface ShoesModelSizeService {

  List<ShoesServiceModel> findAll();

  ShoesServiceModel getShoesModelSizeById(Long id);

  void addSize(Long modelId, int size, int quantity);

  void deleteById(Long id);

  Set<ShoesModelSize> findAllByModelId(Long id);

  void editShoesModelSizeInfo(Long sizeId, BigDecimal price, int quantity);
}
