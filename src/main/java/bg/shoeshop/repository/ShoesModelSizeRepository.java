package bg.shoeshop.repository;

import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.entity.ShoesModelSize;
import bg.shoeshop.model.enums.Sex;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShoesModelSizeRepository extends JpaRepository<ShoesModelSize, Long> {

  <T> Set<T> findBy(Class<T> projection);

  @Override
  Page<ShoesModelSize> findAll(Pageable pageable);

  Page<ShoesModelSize> findAllByModelSex(Sex sex, Pageable pageable);

  List<ShoesModelSize> findAllByModelName(String name);

  Set<ShoesModelSize> findAllByModel(ShoesModel model);

  @Query("select distinct sms.size from ShoesModelSize sms")
  List<Integer> findAllDistinctSizes();

  @Query(
      "select sms.size from ShoesModelSize sms\r\n"
          + "join sms.model sm \r\n"
          + "on sms.id =sm.id \r\n"
          + "where sm.sex = ?1")
  List<Integer> findAllDistinctSizesForSex(Sex sex);

  Optional<ShoesModelSize> findByModelIdAndSize(Long modelId, int size);

  Set<ShoesModelSize> findAllByModelId(Long modelId);
}
