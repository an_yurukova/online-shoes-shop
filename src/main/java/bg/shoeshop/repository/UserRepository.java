package bg.shoeshop.repository;

import bg.shoeshop.model.entity.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

  Optional<User> findByUsername(String username);

  @Override
  Optional<User> findById(Long id);

  //  @Query(
  //      "select sum(sm.price) \r\n"
  //          + "from user_shopping_cart usc \r\n"
  //          + "join ShoesModelSize sms \r\n"
  //          + "on usc.SHOES_MODEL_SIZE_ID = sms.ID\r\n"
  //          + "join ShoesModel sm \r\n"
  //          + "on sms.model.id = sm.id\r\n"
  //          + "where usc.USER_ID =7")
  //  BigDecimal findTotalAmountOfShoppingCart(Long id);
}
