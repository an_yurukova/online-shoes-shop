package bg.shoeshop.repository;

import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.entity.ShoesModelSize;
import bg.shoeshop.model.entity.User;
import java.math.BigDecimal;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

public class UserRepositoryCustomImpl implements UserRepositoryCustom {

  @PersistenceContext private EntityManager entityManager;

  @Override
  public BigDecimal getShoppingCartTotalAmount(Long userId) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<BigDecimal> criteria = cb.createQuery(BigDecimal.class);
    Root<User> root = criteria.from(User.class);
    Join<User, ShoesModelSize> modelSizes = root.join("shoppingCart");
    Join<ShoesModelSize, ShoesModel> shoesModels = modelSizes.join("model");

    criteria.select(cb.sum(shoesModels.get("price"))).where(cb.equal(root.get("id"), userId));

    BigDecimal totalAmount = entityManager.createQuery(criteria).getSingleResult();

    return Optional.ofNullable(totalAmount).orElse(BigDecimal.ZERO);
  }
}
