package bg.shoeshop.repository;

import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import bg.shoeshop.web.ShoesModelsFilter;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface ShoesModelRepository
    extends JpaRepository<ShoesModel, Long>, JpaSpecificationExecutor<ShoesModel> {

  @Query("select distinct type from ShoesModel where sex = ?1")
  List<Type> findAllDistinctTypes(Sex sex);

  @Query("select distinct colorHex from ShoesModel")
  List<String> findAllDistinctColors();

  @Query("select distinct colorHex from ShoesModel where sex = ?1")
  List<String> findAllDistinctColors(Sex sex);

  Optional<ShoesModel> findByName(String name);

  @Query(
      "select sm from ShoesModel sm \r\n"
          + "right join ShoesModelSize sms \r\n"
          + "on sm.id = sms.model.id")
  Page<ShoesModel> findAllAvailableModels(ShoesModelsFilter shoesModelsFilter, Pageable pageable);

  @Query(
      "select distinct sm.type from ShoesModel sm\r\n"
          + "join ShoesModelSize sms\r\n"
          + "on sm.id = sms.model.id")
  List<Type> findAllAvailableModelTypes();

  @Query(
      "select distinct sms.size from ShoesModel sm\r\n"
          + "right join ShoesModelSize sms \r\n"
          + "on sm.id = sms.model.id \r\n"
          + "order by sms.size")
  List<Integer> findAllAvailableSizes();

  @Query(
      "select distinct sm.colorHex from ShoesModel sm\r\n"
          + "join ShoesModelSize sms\r\n"
          + "on sm.id = sms.model.id")
  List<String> findAllAvailableColorHexes();

  @Query(
      "select distinct sm.colorHex from ShoesModel sm \r\n"
          + "right join ShoesModelSize sms \r\n"
          + "on sm.id = sms.model.id\r\n"
          + "where sms.quantity > 0"
          + "and sm.sex = ?1")
  List<String> findAllAvailableColorHexesForSex(Sex sex);

  @Query(
      "select distinct sm from ShoesModel sm \r\n"
          + "right join ShoesModelSize sms \r\n"
          + "on sm.id = sms.model.id\r\n"
          + "where sms.quantity > 0"
          + "and sm.sex = ?1")
  Page<ShoesModel> findAllAvailableShoesModelsForSex(Sex sex, Pageable pageable);

  @Query(
      "select distinct sm.type from ShoesModel sm \r\n"
          + "right join ShoesModelSize sms \r\n"
          + "on sm.id = sms.model.id\r\n"
          + "where sms.quantity > 0"
          + "and sm.sex = ?1")
  List<Type> findAllAvailableModelTypesForSex(Sex sex);

  @Query(
      "select distinct sms.size from ShoesModel sm\r\n"
          + "right join ShoesModelSize sms\r\n"
          + "on sm.id = sms.model.id \r\n"
          + "where sms.quantity > 0\r\n"
          + "and sm.sex = ?1\r\n"
          + "order by sms.size")
  List<Integer> findAllAvailableSizesForSex(Sex sex);

  @Query(
      "select distinct sms.size from ShoesModel sm \r\n"
          + "join ShoesModelSize sms\r\n"
          + "on sm.id = sms.model.id \r\n"
          + "where sm.id = ?1")
  List<Integer> findAllAvailableSizesForShoesModel(Long id);
}
