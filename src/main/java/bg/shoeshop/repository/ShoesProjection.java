package bg.shoeshop.repository;

import org.springframework.beans.factory.annotation.Value;

public interface ShoesProjection {

  Long getId();

  @Value("#{target.model.name}")
  String getName();

  @Value("#{target.color.hexCode}")
  String getColor();
}
