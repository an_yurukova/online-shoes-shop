package bg.shoeshop.repository;

import java.math.BigDecimal;

public interface UserRepositoryCustom {

  BigDecimal getShoppingCartTotalAmount(Long userId);
}
