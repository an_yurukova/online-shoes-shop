 package bg.shoeshop.repository;

 import bg.shoeshop.model.entity.Role;
 import java.util.Optional;
 import org.springframework.data.jpa.repository.JpaRepository;

 public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByName(String string);
 }
