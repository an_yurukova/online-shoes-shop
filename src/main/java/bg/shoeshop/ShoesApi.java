// package bg.shoeshop;
//
// import bg.shoeshop.repository.ShoesProjection;
// import bg.shoeshop.repository.ShoesModelSizeRepository;
// import java.util.Set;
// import lombok.RequiredArgsConstructor;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;
//
// @RestController
// @RequestMapping("/api/v1/shoes")
// @RequiredArgsConstructor
// public class ShoesApi {
//
//  private final ShoesModelSizeRepository shoesRepository;
//
//  @GetMapping
//  public Set<ShoesProjection> listAll() {
//    return shoesRepository.findBy(ShoesProjection.class);
//  }
// }
