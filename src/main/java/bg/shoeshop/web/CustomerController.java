package bg.shoeshop.web;

import bg.shoeshop.model.view.ShoesModelSizeViewModel;
import bg.shoeshop.service.UserService;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {
  private final UserService userService;

  @GetMapping("/favourites")
  public String showFavourites(Model model) {
    model.addAttribute("favourites", userService.getAllFavourites());
    return "favourites";
  }

  @DeleteMapping("/favourites")
  private String deleteAllFavourites() {
    System.out.println("inside all");
    userService.deleteFromFavourites();

    return "redirect:/shoes-models";
  }

  @GetMapping("/shopping-cart")
  private String getShoppingCart(Model model) {
    Set<ShoesModelSizeViewModel> shopingCartContent = userService.getShoppingCart();
    model.addAttribute("shoppingCart", shopingCartContent);
    model.addAttribute("totalAmount", userService.getTotalAmount());
    model.addAttribute("countProducts", shopingCartContent.size());
    return "shopping-cart";
  }

  @PostMapping("/shopping-cart/{id}")
  private String addToCart(@PathVariable Long id) {
    userService.addToShoppingCart(id);

    return "redirect:/customers/shopping-cart";
  }

  @DeleteMapping("/shopping-cart/{id}")
  private String removeFromCartByModelId(@PathVariable Long id) {

    System.out.println("inside deletemapping");
    userService.deleteFromShoppingCartById(id);

    return "redirect:/customers/shopping-cart";
  }

  @DeleteMapping("/shopping-cart")
  private String removeFromCartByModelId() {

    return "redirect:/customers/shopping-cart";
  }
}
