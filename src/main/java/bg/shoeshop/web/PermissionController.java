package bg.shoeshop.web;

import bg.shoeshop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/permissions")
@RequiredArgsConstructor
public class PermissionController {

  private final UserService userService;

  @GetMapping()
  private String showAllUsers(Model model) {
    model.addAttribute("allUsers", userService.getAllUsers());
    model.addAttribute("countUsers", userService.getAllUsers().size());

    return "user";
  }

  @PostMapping("/add/{id}")
  private void addPermission(@PathVariable Long id, Model model) {
    userService.addPermission(id);
  }
}
