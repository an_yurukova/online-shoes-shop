package bg.shoeshop.web;

import bg.shoeshop.aop.LogMethodName;
import bg.shoeshop.aop.MonitorExecutingTime;
import bg.shoeshop.model.binding.ShoesModelBindingModel;
import bg.shoeshop.model.entity.ShoesModel;
import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import bg.shoeshop.model.service.ShoesModelServiceModel;
import bg.shoeshop.model.view.ShoesModelViewModel;
import bg.shoeshop.service.CarouselService;
import bg.shoeshop.service.ShoesModelService;
import bg.shoeshop.service.ShoesModelSizeService;
import java.io.IOException;
import java.util.List;
import javassist.NotFoundException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Controller
@RequestMapping("/shoes-models")
public class ShoesModelController {

  private final ModelMapper modelMapper;
  private final ShoesModelService shoesModelService;
  private final ShoesModelSizeService shoesModelSizeService;
  private final CarouselService carouselService;

  @GetMapping("/admin")
  private String admin() {

    return "edit-shoes";
  }

  @GetMapping("/new")
  public String addShoesModel(Model model) {
    if (!model.containsAttribute("shoesModelBindingModel")) {
      model.addAttribute("shoesModelBindingModel", new ShoesModelBindingModel());
      model.addAttribute("existing", false);
    }
    return "add-model";
  }

  @PostMapping // (consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public String createShoesModel(
      @Valid ShoesModelBindingModel shoesModelBindingModel,
      BindingResult bindingResult,
      RedirectAttributes redirectAttributes)
      throws IOException {

    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute("shoesModelBindingModel", shoesModelBindingModel);
      redirectAttributes.addFlashAttribute(
          "org.springframework.validation.BindingResult.shoesModelBindingModel", bindingResult);

      return "redirect:/shoes-models/new";
    }

    boolean existing = shoesModelService.checkIfExists(shoesModelBindingModel.getName());
    System.out.println(existing);
    if (existing) {
      redirectAttributes.addFlashAttribute("shoesModelBindingModel", shoesModelBindingModel);
      redirectAttributes.addFlashAttribute("existing", true);

      return "redirect:/shoes-models/new";
    }
    ShoesModel shoesModel =
        shoesModelService.addModel(
            modelMapper.map(shoesModelBindingModel, ShoesModelServiceModel.class));

    return "redirect:"
        + UriComponentsBuilder.fromPath("/shoes-models/{id}/sizes/new")
            .buildAndExpand(shoesModel.getId())
            .toUriString();
  }

  @GetMapping("/{id}/sizes/new")
  public String addSizes(@PathVariable Long id, Model model) {
    ShoesModelViewModel shoesModel =
        modelMapper.map(shoesModelService.getById(id), ShoesModelViewModel.class);
    model.addAttribute("shoesModel", shoesModel);

    return "add-model-sizes";
  }

  @GetMapping("/{sex}/{id}")
  public String showShoesModel(@PathVariable Long id, Model model) throws NotFoundException {
    ShoesModelViewModel shoesModel =
        modelMapper.map(shoesModelService.getById(id), ShoesModelViewModel.class);

    model.addAttribute("shoesModel", shoesModel);
    model.addAttribute("allSizes", shoesModelSizeService.findAllByModelId(id));

    return "product";
  }

  @LogMethodName
  @MonitorExecutingTime
  @GetMapping
  public String showAllModels(
      @PageableDefault(size = 9) Pageable pageable,
      Model model,
      ShoesModelsFilter shoesModelsFilter) {
    Page<ShoesModelViewModel> allModels =
        shoesModelService.getAllAvailableShoesModels(shoesModelsFilter, pageable);
    List<Integer> allSizes = shoesModelService.getAllAvailableSizes();
    List<Type> allModelTypes = shoesModelService.getAllAvailableModelTypes();
    List<String> allColorHex = shoesModelService.getAllAvailableColorHexes();

    model.addAttribute("allModels", allModels);
    model.addAttribute("allSizes", allSizes);
    model.addAttribute("allModelTypes", allModelTypes);
    model.addAttribute("allColorHex", allColorHex);
    model.addAttribute("firstImage", carouselService.firstImage());
    model.addAttribute("secondImage", carouselService.secondImage());
    model.addAttribute("thirdImage", carouselService.thirdImage());
    return "index";
  }

  @LogMethodName
  @MonitorExecutingTime
  @GetMapping("/{sex}")
  public String showAllModelsForSex(
      @PathVariable Sex sex,
      @PageableDefault(size = 9) Pageable pageable,
      Model model,
      ShoesModelsFilter shoesModelsFilter) {
    Page<ShoesModelViewModel> allModels =
        shoesModelService.getAllAvailableShoesModelsForSex(sex, pageable, shoesModelsFilter);
    List<Integer> allSizes = shoesModelService.getAllAvailableSizesForSex(sex);
    List<Type> allModelTypesforSex = shoesModelService.getAllAvailableModelTypesForSex(sex);
    List<String> allColorHexForSex = shoesModelService.getAllAvailableColorHexesForSex(sex);

    model.addAttribute("allModels", allModels);
    model.addAttribute("allSizes", allSizes);
    model.addAttribute("allModelTypes", allModelTypesforSex);
    model.addAttribute("allColorHex", allColorHexForSex);
    model.addAttribute("sex", sex);
    return "index";
  }
}
