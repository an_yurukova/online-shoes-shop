package bg.shoeshop.web;
import bg.shoeshop.model.enums.Type;
import java.util.Collections;
import java.util.Set;
import lombok.Data;

@Data
public class ShoesModelsFilter {
  private Set<Type> type = Collections.emptySet();
  private Set<Integer> size = Collections.emptySet();
  private Set<String> colour = Collections.emptySet();
}
