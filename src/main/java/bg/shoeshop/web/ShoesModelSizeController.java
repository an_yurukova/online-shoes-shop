package bg.shoeshop.web;

import bg.shoeshop.model.binding.ShoesModelBindingModel;
import bg.shoeshop.model.binding.ShoesModelSizeBindingModel;
import bg.shoeshop.model.service.ShoesServiceModel;
import bg.shoeshop.service.ShoesModelService;
import bg.shoeshop.service.ShoesModelSizeService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Controller
@RequestMapping("/shoes-model-sizes")
public class ShoesModelSizeController {

  private final ShoesModelService shoesModelService;
  private final ShoesModelSizeService shoesModelSizeService;
  private final ModelMapper modelMapper;

  @GetMapping
  public String allShoesModelSizes(Model model) {
    if (!model.containsAttribute("shoesModelBindingModel")) {
      model.addAttribute("shoesModelBindingModel", new ShoesModelBindingModel());
    }
    return "shoes-model-sizes";
  }

  @GetMapping("/validate")
  public String addShoesModelSizeToTheNewModel(Model model) {
    System.out.println("inside sizecontroller ");
    if (!model.containsAttribute("shoesModel")) {
      model.addAttribute("shoesModel", new ShoesModelSizeBindingModel());
    }
    System.out.println(model.getAttribute("shoesModel").toString());
    return "add-model-sizes";
  }

  @PostMapping
  public String addShoesModelSize(
      @Valid ShoesModelSizeBindingModel shoesModelSizeBindingModel,
      BindingResult bindingResult,
      RedirectAttributes redirectAttributes) {

    Long modelId = shoesModelService.getModelByName(shoesModelSizeBindingModel.getName()).getId();
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute("shoesModel", shoesModelSizeBindingModel);
      redirectAttributes.addFlashAttribute(
          "org.springframework.validation.BindingResult.shoesModel", bindingResult);

      return "redirect:/shoes-model-sizes/validate";
    }

    shoesModelSizeService.addSize(
        modelId, shoesModelSizeBindingModel.getSize(), shoesModelSizeBindingModel.getQuantity());

    return "redirect:"
        + UriComponentsBuilder.fromPath("/shoes-models/{sex}/{id}")
            .buildAndExpand(shoesModelSizeBindingModel.getSex(), modelId)
            .toUriString();
  }

  @GetMapping("/{id}")
  public String shoeModelSizeById(@PathVariable Long id, Model model) {

    if (!model.containsAttribute("shoesModelSizeBindingModel")) {
      ShoesServiceModel sms = shoesModelSizeService.getShoesModelSizeById(id);
      ShoesModelSizeBindingModel shoesModelSizeBindingModel =
          modelMapper.map(sms, ShoesModelSizeBindingModel.class);
      shoesModelSizeBindingModel.setName(sms.getModel().getName());
      shoesModelSizeBindingModel.setPrice(sms.getModel().getPrice());
      shoesModelSizeBindingModel.setType(sms.getModel().getType());
      shoesModelSizeBindingModel.setColorHex(sms.getModel().getColorHex());
      shoesModelSizeBindingModel.setSex(sms.getModel().getSex());
      model.addAttribute("shoesModelSizeBindingModel", shoesModelSizeBindingModel);
    }
    return "edit-shoes-model-size";
  }

  @PostMapping("/{id}")
  public String deleteById(
      @Valid ShoesModelSizeBindingModel shoesModelSizeBindingModel,
      BindingResult bindingResult,
      RedirectAttributes redirectAttributes) {

    Long sizeId = shoesModelSizeBindingModel.getId();
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute(
          "shoesModelSizeBindingModel", shoesModelSizeBindingModel);
      redirectAttributes.addFlashAttribute(
          "org.springframework.validation.BindingResult.shoesModelSizeBindingModel", bindingResult);

      return "redirect:/shoes-model-sizes/" + sizeId;
    }

    shoesModelSizeService.editShoesModelSizeInfo(
        sizeId, shoesModelSizeBindingModel.getPrice(), shoesModelSizeBindingModel.getQuantity());
    return "redirect:/shoes-model-sizes";
  }
}
