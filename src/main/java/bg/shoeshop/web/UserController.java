package bg.shoeshop.web;

import bg.shoeshop.model.binding.UserProfileBindingModel;
import bg.shoeshop.model.binding.UserRegisterBindingModel;
import bg.shoeshop.model.service.UserServiceModel;
import bg.shoeshop.service.UserService;
import java.util.Arrays;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/users")
public class UserController {

  private final ModelMapper modelMapper;
  private final UserService userService;

  public UserController(ModelMapper modelMapper, UserService userService) {
    this.modelMapper = modelMapper;
    this.userService = userService;
  }

  @GetMapping("/login")
  private String login() {

    return "login";
  }

  @PostMapping("/login-error")
  public String failedLogin(
      @ModelAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY)
          String username,
      RedirectAttributes redirectAttributes) {

    redirectAttributes.addFlashAttribute("wrongCredentials", true);
    redirectAttributes.addFlashAttribute("username", username);
    return "redirect:/users/login";
  }

  @GetMapping("/register")
  private String register(Model model) {
    if (!model.containsAttribute("userRegisterBindingModel")) {
      model.addAttribute("userRegisterBindingModel", new UserRegisterBindingModel());
      model.addAttribute("userExists", false);
      model.addAttribute("passwordsNotMatch", false);
    }
    return "register";
  }

  @PostMapping("/register")
  private String registerAndLogin(
      @Valid @ModelAttribute("UserRegisterBindingModel")
          UserRegisterBindingModel userRegisterBindingModel,
      BindingResult bindingResult,
      RedirectAttributes redirectAttributes) {

    if (bindingResult.hasErrors()
        || !Arrays.equals(
            userRegisterBindingModel.getPassword(),
            userRegisterBindingModel.getConfirmPassword())) {
      redirectAttributes.addFlashAttribute("userRegisterBindingModel", userRegisterBindingModel);
      redirectAttributes.addFlashAttribute(
          "org.springframework.validation.BindingResult.userRegisterBindingModel", bindingResult);
      if (!Arrays.equals(
          userRegisterBindingModel.getPassword(), userRegisterBindingModel.getConfirmPassword())) {
        redirectAttributes.addFlashAttribute("passwordsNotMatch", true);
      }
      return "redirect:/users/register";
    }

    if (userService.usernameExists(userRegisterBindingModel.getUsername())) {
      redirectAttributes.addFlashAttribute("userRegisterBindingModel", userRegisterBindingModel);
      redirectAttributes.addFlashAttribute("userExists", true);
      return "redirect:/users/register";
    }
    UserServiceModel userServiceModel =
        modelMapper.map(userRegisterBindingModel, UserServiceModel.class);

    userService.registerAndLogin(userServiceModel);
    return "redirect:/";
  }

  @GetMapping("/update/{id}")
  public String addPermission(@PathVariable Long id) {
    userService.addPermission(id);
    return "redirect:/";
  }

  @GetMapping("/user-info")
  public String userInfo(Model model) {
    if (!model.containsAttribute("userProfileBindingModel")) {
      model.addAttribute("userProfileBindingModel", userService.getUser());
    }
    return "profil";
  }

  @PostMapping("/update")
  public String changeUserInfo(
      @Valid UserProfileBindingModel userProfileBindingModel,
      BindingResult bindingResult,
      RedirectAttributes redirectAttributes) {

    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute("userProfileBindingModel", userProfileBindingModel);
      redirectAttributes.addFlashAttribute(
          "org.springframework.validation.BindingResult.userProfileBindingModel", bindingResult);
      return "redirect:/users/user-info";
    }

    UserServiceModel userServiceModel =
        modelMapper.map(userProfileBindingModel, UserServiceModel.class);

    userService.changeUserInfo(userServiceModel);

    return "redirect:/users/user-info";
  }
}
