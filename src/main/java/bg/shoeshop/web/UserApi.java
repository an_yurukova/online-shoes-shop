package bg.shoeshop.web;

import bg.shoeshop.model.binding.UserUpdateBindingModel;
import bg.shoeshop.model.view.UserViewModel;
import bg.shoeshop.service.RoleService;
import bg.shoeshop.service.UserService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UserApi {

  private final UserService userService;
  private final RoleService roleService;

  @GetMapping
  public List<UserViewModel> getAllUsers() {
    return userService.getAllUsers();
  }

  @PutMapping("/{id}")
  public void updateUser(
      @PathVariable Long id, @RequestBody UserUpdateBindingModel userUpdateBindingModel) {
    userService.update(id, userUpdateBindingModel);
  }

  @GetMapping("/roles")
  public Set<String> getAllRoles() {
    return roleService.getAllRoles();
  }

  @PostMapping("/updateRole/{id}")
  public void updateRoles(
      @PathVariable Long id, @RequestBody UserUpdateBindingModel userUpdateBindingModel) {

    userService.updateRoles(id, userUpdateBindingModel.getRoleName());
  }
}
