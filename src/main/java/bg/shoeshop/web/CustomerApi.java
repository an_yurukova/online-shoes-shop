package bg.shoeshop.web;

import bg.shoeshop.service.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomerApi {

  private final UserService userService;

  @PostMapping("/shopping-cart/items")
  public void addShoppingCartItem(@RequestBody ShoppingCartItem item) {
    userService.addToShoppingCart(item.getId());
  }

  @GetMapping("/shopping-cart/items-count")
  public int getShoppingCartItemsCount() {
    return userService.getShoppingCart().size();
  }

  @Data
  public static class ShoppingCartItem {
    private Long id;
  }

  @PostMapping("/favourites/items")
  public void addFavouritesItem(@RequestBody ShoppingCartItem item) {
    userService.addToFavourites(item.getId());
  }
}
