package bg.shoeshop.web;

import bg.shoeshop.model.view.ShoesModelSizeViewModel;
import bg.shoeshop.service.ShoesModelSizeService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/shoes-model-sizes")
public class ShoesModelSizeApi {

  private final ShoesModelSizeService shoesModelSizeService;
  private final ModelMapper modelMapper;

  @GetMapping
  public List<ShoesModelSizeViewModel> findAll() {
    return shoesModelSizeService
        .findAll()
        .stream()
        .map(m -> modelMapper.map(m, ShoesModelSizeViewModel.class))
        .collect(Collectors.toList());
  }

  @DeleteMapping("/{id}")
  public void deleteById(@PathVariable Long id) {
    shoesModelSizeService.deleteById(id);
  }
}
