package bg.shoeshop.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class RedirectHomeInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {

    System.out.println("Request URL: " + request.getRequestURL());
    System.out.println("Sorry! This URL is no longer used, Redirect to /home");

    response.sendRedirect(request.getContextPath() + "/home");
    return false;
  }
}
