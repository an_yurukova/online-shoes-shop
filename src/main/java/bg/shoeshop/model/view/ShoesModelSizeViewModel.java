package bg.shoeshop.model.view;

import bg.shoeshop.model.entity.ShoesModel;
import lombok.Data;

@Data
public class ShoesModelSizeViewModel {

  private Long id;
  private ShoesModel model;
  private int size;
  private int quantity;
}
