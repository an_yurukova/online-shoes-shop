package bg.shoeshop.model.view;

import java.util.List;
import lombok.Data;

@Data
public class UserRestViewModel {
  private Long id;
  private String username;
  private String firstName;
  private String lastName;
  private String email;
  private boolean active;
  private List<String> roleNames;
}
