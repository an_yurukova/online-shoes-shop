package bg.shoeshop.model.view;

import bg.shoeshop.model.entity.Role;
import java.util.List;
import lombok.Data;

@Data
public class UserViewModel {
  private Long id;
  private String username;
  private String firstName;
  private String lastName;
  private String email;
  private boolean active;
  private List<Role> roles;
}
