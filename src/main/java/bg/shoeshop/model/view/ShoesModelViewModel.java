package bg.shoeshop.model.view;

import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.Data;

@Data
public class ShoesModelViewModel {
  private Long id;
  private String name;
  private Type type;
  private Sex sex;
  private String colorHex;
  private BigDecimal price;
  private LocalDate createdOn;
  private String imageUrl;
  //  private Set<Integer> sizes = new HashSet<>();
}
