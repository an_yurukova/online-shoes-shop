package bg.shoeshop.model.enums;

public enum Type {
  BALLERINAS,
  SANDALS,
  SNEAKERS,
  PUMPS,
  BOOTS
}
