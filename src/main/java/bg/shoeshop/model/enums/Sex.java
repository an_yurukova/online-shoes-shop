package bg.shoeshop.model.enums;

public enum Sex {
  MALE,
  FEMALE
}
