package bg.shoeshop.model.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ShoesModelSize extends BaseEntity {

  @ManyToOne @JoinColumn private ShoesModel model;

  private int size;

  private int quantity;
}
