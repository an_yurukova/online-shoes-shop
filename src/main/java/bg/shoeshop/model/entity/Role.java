package bg.shoeshop.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.Data;

@Data
@Entity
public class Role extends BaseEntity {

  private String name;

  @ManyToMany
  @JsonIgnore
  @JoinTable(
      name = "ROLE_AUTHORITIES",
      joinColumns = @JoinColumn(name = "ROLE_ID"),
      inverseJoinColumns = @JoinColumn(name = "AUTHORITY_ID"))
  private Set<Authority> authorities = new HashSet<>();
}
