package bg.shoeshop.model.entity;

import javax.persistence.Entity;
import lombok.Data;

@Data
@Entity
public class Authority extends BaseEntity {

  private String name;
}
