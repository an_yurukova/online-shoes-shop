package bg.shoeshop.model.entity;

import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class ShoesModel extends BaseEntity {

  private String name;

  @Enumerated(EnumType.STRING)
  private Type type;

  @Enumerated(EnumType.STRING)
  private Sex sex;

  private String colorHex;

  private BigDecimal price;

  private LocalDate createdOn;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID")
  @JsonIgnore
  private User createdBy;

  private String imageUrl;

  @OneToMany(mappedBy = "model")
  @JsonIgnore
  private Set<ShoesModelSize> sizes;
}
