package bg.shoeshop.model.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class User extends BaseEntity {

  private String username;
  private String firstName;
  private String lastName;
  private String email;
  private char[] password;
  private boolean active;

  @ManyToMany
  @JoinTable(
      joinColumns = @JoinColumn(name = "USER_ID"),
      inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
  private List<Role> roles = new ArrayList<Role>();

  @ManyToMany
  @JoinTable(
      joinColumns = @JoinColumn(name = "USER_ID"),
      inverseJoinColumns = @JoinColumn(name = "SHOES_MODEL_SIZE_ID", referencedColumnName = "ID"))
  private Set<ShoesModelSize> favourites = new HashSet<>();

  @ManyToMany
  @JoinTable(
      joinColumns = @JoinColumn(name = "USER_ID"),
      inverseJoinColumns = @JoinColumn(name = "SHOES_MODEL_SIZE_ID", referencedColumnName = "ID"))
  private Set<ShoesModelSize> shoppingCart = new HashSet<>();

  @PreRemove
  public void preRemove() {
    roles.clear();
    favourites.clear();
    shoppingCart.clear();
  }
}
