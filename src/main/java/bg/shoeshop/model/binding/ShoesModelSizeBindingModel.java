package bg.shoeshop.model.binding;

import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class ShoesModelSizeBindingModel {
  private Long id;

  @NotBlank(message = "Cannot be empty")
  @Size(min = 3, max = 20, message = "Name length must be between 3 and 20 characters")
  private String name;

  @NotNull private Sex sex;

  @NotNull private Type type;

  //  @NotBlank(message = "Cannot be empty")
  private String colorHex;

  @DecimalMin(value = "1", message = "Price must be a positive number")
  private BigDecimal price;

  @Min(value = 36)
  @Max(value = 45)
  private int size;

  @Min(value = 1)
  private int quantity;
}
