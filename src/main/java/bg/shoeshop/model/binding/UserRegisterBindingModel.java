package bg.shoeshop.model.binding;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class UserRegisterBindingModel {

  @NotBlank(message = "Username cannot be empty")
  @Size(min = 3, max = 20, message = "Username length must be between 3 and 20 characters.")
  private String username;

  @NotBlank(message = "First name cannot be empty")
  @Size(min = 3, max = 20, message = "First name length must be between 3 and 20 characters.")
  private String firstName;

  @NotBlank(message = "Last name cannot be empty")
  @Size(min = 3, max = 20, message = "Last name length must be between 3 and 20 characters.")
  private String lastName;

  @NotBlank
  @Email(message = "Email must be valid.")
  private String email;

  @Size(min = 5, max = 20, message = "Password length must be between 5 and 20 characters.")
  private char[] password;

  private char[] confirmPassword;
}
