package bg.shoeshop.model.binding;

import lombok.Data;

@Data
public class UserUpdateBindingModel {
  private boolean active;
  private String roleName;
}
