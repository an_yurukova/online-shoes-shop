package bg.shoeshop.model.binding;

import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import bg.shoeshop.validation.NotNullMultipart;
import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ShoesModelBindingModel {
  @NotBlank(message = "Cannot be empty")
  @Size(min = 3, max = 20, message = "Name length must be between 3 and 20 characters")
  private String name;

  @NotNull private Sex sex;

  @NotNull private Type type;

  @NotBlank(message = "Cannot be empty")
  private String colorHex;

  @NotNull
  @DecimalMin(value = "1", message = "Price must be a positive number")
  private BigDecimal price;

  @NotNullMultipart private MultipartFile imageUrl;
}
