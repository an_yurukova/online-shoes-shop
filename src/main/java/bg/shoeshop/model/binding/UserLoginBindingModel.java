package bg.shoeshop.model.binding;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class UserLoginBindingModel {
  @NotBlank(message = "Username cannot be empty")
  @Size(min = 3, max = 20, message = "Username length must be between 3 and 20 characters.")
  private String username;

  @NotBlank
  @Size(min = 5, max = 20, message = "Password length must be between 5 and 20 characters.")
  private char[] password;
}
