package bg.shoeshop.model.service;

import lombok.Data;

@Data
public class UserServiceModel {

  private Long id;
  private String username;
  private String firstName;
  private String lastName;
  private char[] password;
  private String email;
}
