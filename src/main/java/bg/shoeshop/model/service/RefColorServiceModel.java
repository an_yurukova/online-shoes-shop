package bg.shoeshop.model.service;

import lombok.Data;

@Data
public class RefColorServiceModel {

  private Long id;
  private String hexCode;

  public RefColorServiceModel() {}

  public RefColorServiceModel(String hexCode) {
    this.hexCode = hexCode;
  }
}
