package bg.shoeshop.model.service;

import bg.shoeshop.model.entity.ShoesModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShoesServiceModel {

  private Long id;

  private ShoesModel model;
  private int size;
  private int quantity;
}
