package bg.shoeshop.model.service;

import bg.shoeshop.model.enums.Sex;
import bg.shoeshop.model.enums.Type;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ShoesModelServiceModel {

  private String name;
  private Type type;
  private Sex sex;
  private String colorHex;
  private BigDecimal price;
  private LocalDate addedOn;
  private MultipartFile imageUrl;
  private Set<Integer> sizes = new HashSet<>();
}
