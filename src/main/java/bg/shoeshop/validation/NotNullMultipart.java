package bg.shoeshop.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target(FIELD)
@Constraint(validatedBy = MultipartFileConstraintValidator.class)
public @interface NotNullMultipart {
  String message() default "File is required";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
