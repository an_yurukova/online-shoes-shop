package bg.shoeshop.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.web.multipart.MultipartFile;

public class MultipartFileConstraintValidator
    implements ConstraintValidator<NotNullMultipart, MultipartFile> {

  @Override
  public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
    if (!value.isEmpty()) {
      return true;
    }

    return false;
  }
}
