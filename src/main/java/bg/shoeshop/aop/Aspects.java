package bg.shoeshop.aop;

import java.util.Arrays;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Aspects {
  @Before("@annotation(bg.shoeshop.aop.LogMethodName)")
  public void logMethodName(JoinPoint joinPoint) {
    String methodName = joinPoint.getSignature().getName();
    String params = Arrays.toString(joinPoint.getArgs());
    System.out.println("Method [" + methodName + "] gets called with parameters " + params);
  }

  @Around("@annotation(bg.shoeshop.aop.MonitorExecutingTime)")
  public Object monitorTime(ProceedingJoinPoint pjp) throws Throwable {
    long startTime = System.currentTimeMillis();
    Object proceed = pjp.proceed();
    long duration = System.currentTimeMillis() - startTime;
    System.out.println("Execution took [" + duration + "ms]");
    return proceed;
  }
}
