package bg.shoeshop.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerClass {

  @ExceptionHandler(value = ProductNotFoundException.class)
  public ModelAndView handleProductNotFoundException(ProductNotFoundException ex) {
    ModelAndView mv = new ModelAndView("error-404");
    System.out.println("inside ExceptionHandlerClass");
    return mv.addObject("message", ex.getMessage());
  }
}
