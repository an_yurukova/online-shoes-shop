package bg.shoeshop.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GeneralExceptionHandler {

  @ExceptionHandler(value = Exception.class)
  public String handleException(Exception exception) {
    System.out.println("inside GeneralExceptionhandler");
    log.warn("Error: ", exception);
    return "exception";
  }
}
