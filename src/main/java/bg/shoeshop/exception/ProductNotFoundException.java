package bg.shoeshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The product is currently not available")
public class ProductNotFoundException extends RuntimeException {

  /** */
  private static final long serialVersionUID = 1L;

  public ProductNotFoundException(String product) {
    super(product + " not available.");
  }
}
