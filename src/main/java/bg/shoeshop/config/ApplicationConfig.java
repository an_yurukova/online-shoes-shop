package bg.shoeshop.config;

import bg.shoeshop.service.CloudinaryService;
import java.io.IOException;
import java.io.UncheckedIOException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@Configuration
public class ApplicationConfig {

  private final CloudinaryService cloudinaryService;

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper mapper = new ModelMapper();

    mapper
        .createTypeMap(MultipartFile.class, String.class)
        .setConverter(multipartUploadConverter());
    return mapper;
  }

  private Converter<MultipartFile, String> multipartUploadConverter() {
    return ctx -> {
      try {
        return cloudinaryService.uploadImage(ctx.getSource());
      } catch (IOException e) {
        throw new UncheckedIOException(e);
      }
    };
  }
}
