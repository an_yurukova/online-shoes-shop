package bg.shoeshop.config;

import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Setter
@ConfigurationProperties
public class CloudinaryProperties {

  @NestedConfigurationProperty private Map<String, Object> cloudinary = new LinkedHashMap<>();

  public Map<String, Object> getConfig() {
    return cloudinary;
  }
}
