package bg.shoeshop.config;

import bg.shoeshop.interceptor.LogInterceptor;
import bg.shoeshop.interceptor.RedirectHomeInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RequiredArgsConstructor
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

  private final LogInterceptor logInterceptor;
  private final RedirectHomeInterceptor redirectHomeInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(logInterceptor);
    registry.addInterceptor(redirectHomeInterceptor).addPathPatterns("/");
  }
}
