package bg.shoeshop.config;

import com.cloudinary.Cloudinary;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(CloudinaryProperties.class)
public class CloudinaryConfig {

  @Bean
  public Cloudinary cloudinary(CloudinaryProperties cloudinaryProperties) {
    return new Cloudinary(cloudinaryProperties.getConfig());
  }
}
